Sheep Farm at
10491 Perry Lake Rd
Ortonville, MI 48462

Phone: Pixel
Drone: Mavic 2 Zoom

Altitude between 70-100 feet

These videos were recorded from the DJI app.
Videos were saved on both the Mavic drone SD card and the Android Pixel phone from default settings.
The recording went to a length of about 21 minutes.
When the battery went to 24%, the app had a pop up that advised me to return the drone to the home point so I did.
The 1 video recorded was broken up into multiple videos that I had no influence over.
